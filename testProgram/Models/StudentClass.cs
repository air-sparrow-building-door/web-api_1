using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testProgram.Models
{
    public class StudentClass
    {
        
        public string? StuName{get;set;}
        public string? StuGender{get;set;}
        public string? ClassName{get;set;}
    }
}