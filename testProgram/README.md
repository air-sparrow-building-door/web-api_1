// See https://aka.ms/new-console-template for more information
//Console.WriteLine("Hello, World!");

//集合: 数组是最原始的集合， 数组：有下标，长度不可变    集合：长度可变：增删，同类型的数据


//创建一个集合
//var lst = new List<int>(){1,2,3,4,5,6,7,8,9,0};  

/* var arr = new int[2]{12,23};
//添加
lst.Add(11);
lst.AddRange(arr);
//遍历
foreach (var item in lst)
{
    Console.Write(item);
    Console.WriteLine();
}

//查找
var num = lst.Find(x=>x%2==0);
Console.WriteLine(num);

//清空所有元素
lst.Clear(); */

//筛选偶数，并从小到大排序


/* lst.Sort(); 

var list= new List<int>();

foreach (var item in lst)
{
    if(item %2==0){
        list.Add(item);
    }
}
list.ForEach(i=>System.Console.WriteLine(i)); */

//linq：
//查询表达式: 以select，group by结尾  T-SQL:  select *  from lst where n%2==0 orderby n desc 
// var res = from n in lst
//           where n%2==0
//           orderby n descending
//           select n;


//链式表达式：js链式调用
//MVC：增删改查
//var lst = new List<int>(){1,2,3,4,5,6,7,8,9,0};  
// var res1 = lst.Where(x=> x%2==0).OrderBy(x=>x).Select(x=>x).ToList();

//linq: 延迟性(defer)， 消耗性(exhaust)
var lst = new List<int>(){1,2,3,4,5,6,7,8,9,0};  
//列表中的每个元素乘方
/* var res = from n in lst
          select n*n; */

lst.Add(10);

//res.Take(n):取前n项
//单个，取一条数据，  first
// Console.WriteLine(res3);


var rnd = new Random(1000);
//集合
var arr = Enumerable.Range(0,100).Select(_ => rnd.Next(0,10));
//dictionary
Console.WriteLine(arr);

        





var rnd = new Random(1000);
var arr = Enumerable.Range(0,100).Select(_ => rnd.Next(0,10));

int[] targetArr=new int[10]; //长度为10的空数组

foreach(var n in arr){
    targetArr[n]++;
}

for(int i=0;i<targetArr.Length;i++){
    Console.WriteLine($"位置[{i}]的个数:"+targetArr[i]);
}

/* var count = 0;  //out
//字典 key value
var dic = new Dictionary<int,int>();
for (int i = 0; i < 10; i++)
{
    if(arr.Contains(i)){
        
    }
} */

/* static void Count(out int count){
    count =0;
    count++;
} */

//Console.WriteLine(String.Join(" ",dic));

//查询表达式，链式（可枚举类型的扩展方法）

var res=   from n in arr
           group n by n into g
		   select new {g.Key,Count = g.Count()};  //g.Count()

// var res1 = arr.GroupBy(n=> n).ToDictionary(n=> n.Key,n=> n.Count());
var res1 = arr.GroupBy(n=> n).Select(g=> new {g.Key,Count=g.Count()});

//
var arr1 = new int[][]{
    new int[]{1,2,3},
    new int[]{4,5,6},
    new int[]{7,8,9}
};

//查询
/* var res2 =  from n in arr1
            from s in n
            select s; */

//扩展方法
// var res2 = arr1.SelectMany(s => s);

var str = new string[]{"rust","csharp","python","java","c","golang"};
//统计每个字符出现的次数,并且按照出现次数频率从高到低排序
var res3 =  from n in str
            from c in n
            group c by c into g
            orderby g.Count() descending
            select new {g.Key,Count = g.Count()}; 

//改成扩展方法       
//linq to object
//linq to sql
//linq to xml