﻿using testProgram.Models;

var stuList = new List<Sutdent>(){
new Sutdent(){StuId=1,StuName="周飘",StuGender=true,ClassId=1},
new Sutdent(){StuId=2,StuName="张飞",StuGender=false,ClassId=2},
new Sutdent(){StuId=3,StuName="刘备",StuGender=false,ClassId=1},
new Sutdent(){StuId=4,StuName="关羽",StuGender=false,ClassId=2}
};

var clsList = new List<Classes>(){
    new Classes(){ClassId=1,ClassName=".net2班"},
    new Classes(){ClassId=2,ClassName=".net1班"},
};

//查询表达式
/* 
    select * from stuList s join clsList c on s.ClassId=c.ClassId (where) 
 */

//班级+姓名+性别
//惰性
/* var res4 =  from s in stuList
            join c in clsList on s.ClassId equals c.ClassId  //equals
            select new StudentClass{
                StuName = s.StuName,
                StuGender = s.StuGender ? "女":"男",
                ClassName = c.ClassName
            }; */

//链式
/* var res4 = stuList.Join(clsList, s => s.ClassId, c => c.ClassId, (s, c) => new StudentClass
{
    StuName = s.StuName,
    StuGender = s.StuGender ? "女" : "男",
    ClassName = c.ClassName

}
);

foreach (var item in res4)
{
    Console.WriteLine($"{item.StuName},{item.StuGender}");
}
 */

//linq不仅仅是针对可枚举类型（Enumerable/IEnumerable）可查询: Queryable,ParallelQuery,OrderdedEnumerable

/* var list = new List<int> { 1, 2, 3, 4, 5 };
var res5 = list
.AsParallel() //多线程计算
.AsOrdered()
.Select(
    (n) =>
    {
        Thread.Sleep(500);
        return n * n;
    }
); */



for (int i = 0; i < 5; i++)
{
    for (int j = 0; j < 4; j++)
    {
        for (int k = 0; k < 3; k++)
        {
            Console.WriteLine($"{i},{j},{k}");
        }
    }
}
//使用linq打印所有排列组合  for i in 
//  var arr = Enumerable.Range(0,5);

var res =   from i in Enumerable.Range(0,5)
            from j in Enumerable.Range(0,4)
            from k in Enumerable.Range(0,3)
            select $"({i},{j},{k})";





