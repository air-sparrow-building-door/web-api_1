using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace _01WebAPI初识.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]  //路由通配规则 
    //参数
    public class TestController : ControllerBase
    {
        [HttpGet]
        public string Get(string name,string age){  //控制器动作
            return $"{age}岁的{name},你好";
        }

        [HttpGet]
        public string Get1(){
            return "我很好";
        }
        [HttpGet]
        public string Get2(){
            return "我很好!!";
        }
    }
}